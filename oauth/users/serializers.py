from rest_framework import serializers
from users.models import User

class UserSerializer( serializers.ModelSerializer ):
    """ Serializa los campos de nuestra APIView """
    # class Meta:
    #     model = User
    #     fields = [ "name", "last_name", "email", "role", "created_at", "updated_at" ]
    
    name = serializers.CharField( max_length = 60 )
    last_name = serializers.CharField( max_length = 60 )
    email = serializers.EmailField()
    role = serializers.CharField( max_length = 20, required = False )
    created_at = serializers.DateTimeField( required = False )
    updated_at = serializers.DateTimeField( required = False )
    
    # return all fields model
    class Meta:
        model = User
        fields = (
            'id',
            'name',
            'last_name',
            'email',
            'password',
            'role',
            'created_at',
            'updated_at'
        )
        
        # These fields are displayed but not editable and have to be a part of 'fields' tuple
        read_only_fields = ( 'id', )

        # These fields are only editable (not displayed) and have to be a part of 'fields' tuple
        # extra_kwargs = { 'password': { 'write_only': True, 'min_length': 8 } }
        