from rest_framework.views import APIView
from rest_framework.response import Response
from users.models import User
from users import serializers
from rest_framework.renderers import JSONRenderer
from rest_framework.parsers import MultiPartParser, FormParser, JSONParser

# Create your views here.
class UserApiView( APIView ):
    """ API View de Usuarios """
    parser_classes = ( MultiPartParser, FormParser, JSONParser )
    serializers_class = serializers.UserSerializer
    
    def get( self, request, format = None ):
        """ Retornar lista de usuarios """
        
        users = User.objects.all()
        serializer = self.serializers_class( users, many = True )
        # json = JSONRenderer().render(serializer.data)
        
        data = {
            'code': 200,
            'status': 'success',
            'message': 'Operation Successfull',
            'data': serializer.data
        }
        
        return Response( data )
    
    def post( self, request ):
        """ Crea un usuario """
        
        output = {
            'code': 0,
            'status': 'error'
        }
        
        serializer = self.serializers_class( data = request.data )
    
        if serializer.is_valid():
            data = serializer.data
                        
            #serializer.save( role = 'user' )
            User.objects.create_user( data[ 'email' ], data[ 'name' ], data[ 'last_name' ], data[ 'password' ] )
            
            output = {
                'code': 200,
                'status': 'success',
                'message': 'Operation Successfull',
                #'data': data
            }
        else:
            output[ 'message' ] = serializer.errors
            
        return Response( output ) 
    
    def put( self, request, pk=None ):
        """Actualizacion objecto"""
        return Response( { 'method': 'PUT' } )
    
    def patch( self, request, pk=None ):
        """Actualización parcial"""
        return Response( { 'method': 'PATCH' } )
    
    def delete( self, request, pk=None ):
        """Borrar objeto"""
        return Response( { 'method': 'DELETE' } )