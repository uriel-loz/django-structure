from django.urls import path

from users import views

urlpatterns = [
    path( 'users-view/', views.UserApiView.as_view() ),
] 